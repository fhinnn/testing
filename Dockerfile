FROM php:8.2-fpm-alpine

WORKDIR /var/www/app
COPY . .
RUN cp ./src/.env.example ./src/.env

RUN apk update && apk add \
    curl \
    libpng-dev \
    libxml2-dev \
    zip \
    unzip

RUN docker-php-ext-install pdo pdo_mysql \
    && apk --no-cache add nodejs npm

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN chmod 777 -R /var/www/app/src && cd src && composer install --no-dev --no-progress --prefer-dist --no-interaction --optimize-autoloader && php artisan key:generate && apk add nginx && cd .. && cp ./nginx/default.conf /etc/nginx/http.d/default.conf

USER root

CMD /usr/sbin/nginx -g "daemon off;" & /usr/local/sbin/php-fpm --nodaemonize

